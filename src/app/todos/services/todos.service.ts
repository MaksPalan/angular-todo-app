import { Injectable } from '@angular/core';
import { rejects } from 'assert';
import { Todo } from '../models/todo';

const DUMMY_TODOS: Todo[] =  [
  {
      id: '1',
      title: 'Task 1',
      completed: true
  },
  {
    id: '2',
    title: 'Task 2',
    completed: false
},
{
    id: '3',
    title: 'Task 3',
    completed: false
}
]

@Injectable({
  providedIn: 'root'
})

export class TodosService {
  todos = DUMMY_TODOS;
  constructor() { }

  getTodos(): Promise<Todo[]> {
    return new Promise((resolve, reject) => {
      resolve(this.todos);
    });
  }

  addTodo(title: string): Promise<Todo> {
    if (!title) { 
      return;
    }
    const newTodo: Todo = {
      id: new Date().getTime().toString(),
      title,
      completed: false
    }
    this.todos = [...this.todos, newTodo];
    return new Promise((resolve, reject) => {
      resolve(newTodo);
    });
  }

  delTodo(id: string): Promise<Todo> {
    const deletedTodo = this.todos.find(todo => todo.id === id);
    // this.todos = this.todos.filter(todo => todo.id !== id);
    return new Promise((resolve, reject) => {
      resolve(deletedTodo);
    });
  }
}
