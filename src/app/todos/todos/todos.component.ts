import { Component, OnInit } from '@angular/core';
import { Todo } from '../models/todo';
import { TodosService } from '../services/todos.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})

export class TodosComponent implements OnInit {
  todos: Todo[];
  newTodoTitle: string;

  constructor(private todosService: TodosService) {
    // this.todosService.getTodos().then(todos => {
    //   console.log(todos);
    // });
    // this.todosService.addTodo('New Todo').then((createdTodo) => {
    //   console.log(createdTodo);
    // });
  }

  ngOnInit(): void {
    this.todosService.getTodos().then(todos => {
      this.todos = todos;
    });
  }

  addTodo(event: Event) {
    event.preventDefault();
    const title = this.newTodoTitle;

    this.todosService.addTodo(title).then(newTodo => {
      this.todos = [...this.todos, newTodo];
      this.newTodoTitle = '';
    });  
  }

  delTodo(id: string) {
    this.todosService.delTodo(id).then(deletedTodo => {
      this.todos = this.todos.filter(todo => todo.id !== id);
      console.log(deletedTodo);
    });
  }
}
